data "aws_ami" "ami" {
  most_recent = true
  owners      = ["self"]
  name_regex  = "^WebApp"
}